modded class MissionServer
{
	string m_CfgActionTime;
	string m_CfgDropRuined;
	
	
	void MissionServer()
	{
		Print("[dzr_rags_by_hand] ::: Starting Serverside");
		GetRPCManager().AddRPC( "DZR_RBH_RPC", "DZR_MissionServer_SendCfgValueToClient", this, SingleplayerExecutionType.Both );
		DZRRBH_MissionServer_CheckFolders("DZR", "dzr_rags_by_hand");
		
		m_CfgActionTime = DZR_ReadCfg("DZR\\dzr_rags_by_hand\\","CfgActionTime.txt");
		m_CfgDropRuined = DZR_ReadCfg("DZR\\dzr_rags_by_hand\\","CfgDropRuined.txt");
		GetDayZGame().DZRRBH_SaveConfigOnServer(m_CfgActionTime.ToInt(), m_CfgDropRuined.ToInt());
	}
	
	void WriteDefaultCfg(string path, int value)
	{
		if(!FileExist(path))
		{
			//Print("File not ixistent: "+ path);
			FileHandle file = OpenFile(path, FileMode.WRITE);
			//FileHandle file = OpenFile("$profile:testiik.txt", FileMode.APPEND);
			if (file != 0)
			{
				//Print("Created: "+ path);
				FPrintln(file, value);
				//FPrintln(file, "line2");
				//FPrintln(file, "line3");
				CloseFile(file);
			}
		}
		else
		{
			//Print(path+ "Exists!");
		}
	}
	
	void DZRRBH_MissionServer_CheckFolders(string dzr_Rbh_TagFolder = "DZR", string dzr_Rbh_ModFolder = "UndefinedModFolder", string dzr_Rbh_SubFolder = "", string dzr_Rbh_SubFolder2 = "")
	{
		string dzr_Rbh_ProfileFolder = "$profile:\\";
		
		
		
		//Print("[dzr_rags_by_hand] ::: DZR_MissionServer_ReadConfigFile");
		if (!FileExist(dzr_Rbh_ProfileFolder+dzr_Rbh_TagFolder+"\\")){
			//NO DZR
			//Print("[dzr_rags_by_hand] ::: DZR_MissionServer_ReadConfigFile NO DZR");
			MakeDirectory(dzr_Rbh_ProfileFolder+dzr_Rbh_TagFolder+"\\");	
			MakeDirectory(dzr_Rbh_ProfileFolder+dzr_Rbh_TagFolder+"\\"+dzr_Rbh_ModFolder+"\\");	
			//dzr_writeConfigFile();	
			WriteDefaultCfg(dzr_Rbh_ProfileFolder+dzr_Rbh_TagFolder+"\\"+dzr_Rbh_ModFolder+"\\CfgActionTime.txt", 20);
			WriteDefaultCfg(dzr_Rbh_ProfileFolder+dzr_Rbh_TagFolder+"\\"+dzr_Rbh_ModFolder+"\\CfgDropRuined.txt", 1);
		}
		else 
		{
			//YES DZR
			//Print("[dzr_rags_by_hand] ::: DZR_MissionServer_ReadConfigFile YES DZR");
			if (!FileExist(dzr_Rbh_ProfileFolder+dzr_Rbh_TagFolder+"\\"+dzr_Rbh_ModFolder+"\\")){
				//NO RBH
				//Print("[dzr_rags_by_hand] ::: DZR_MissionServer_ReadConfigFile NO RBH");
				
				MakeDirectory(dzr_Rbh_ProfileFolder+dzr_Rbh_TagFolder+"\\"+dzr_Rbh_ModFolder+"\\");
				WriteDefaultCfg(dzr_Rbh_ProfileFolder+dzr_Rbh_TagFolder+"\\"+dzr_Rbh_ModFolder+"\\CfgActionTime.txt", 20);
				WriteDefaultCfg(dzr_Rbh_ProfileFolder+dzr_Rbh_TagFolder+"\\"+dzr_Rbh_ModFolder+"\\CfgDropRuined.txt", 1);
				
				//dzr_writeConfigFile();
			}
			else 
			{
				if (!FileExist(dzr_Rbh_ProfileFolder+dzr_Rbh_TagFolder+"\\"+dzr_Rbh_ModFolder+"\\"+dzr_Rbh_SubFolder+"\\") && dzr_Rbh_SubFolder != ""){
					MakeDirectory(dzr_Rbh_ProfileFolder+dzr_Rbh_TagFolder+"\\"+dzr_Rbh_ModFolder+"\\"+dzr_Rbh_SubFolder);
					
					if (!FileExist(dzr_Rbh_ProfileFolder+dzr_Rbh_TagFolder+"\\"+dzr_Rbh_ModFolder+"\\"+dzr_Rbh_SubFolder2+"\\") && dzr_Rbh_SubFolder2 != ""){
						MakeDirectory(dzr_Rbh_ProfileFolder+dzr_Rbh_TagFolder+"\\"+dzr_Rbh_ModFolder+"\\"+dzr_Rbh_SubFolder+"\\"+dzr_Rbh_SubFolder2+"\\");
					}
					
				}
			}
			
		};
		
		
		
		//PROPER CONFIG
		
	}
	
	string DZR_ReadCfg(string m_Path, string m_TxtFileName)
	{
		//ref array<string> compressed = new array<string>;
		//JsonSaveData packed_text_object = new JsonSaveData();
		
		// READ FILE CONTENTS
		string content;
		FileHandle fhandle;
		if (FileExist(m_Path+m_TxtFileName))
		{
			Print(m_Path+m_TxtFileName+" EXISTS!");
			fhandle	=	OpenFile(m_Path+m_TxtFileName, FileMode.READ);
			string line_content;
			while ( FGets( fhandle,  line_content ) > 0 )
			{
				content += line_content;
				Print(line_content);
			}
			CloseFile(fhandle);
		}
		else
		{
			Print("[dzr_rags_by_hand] ::: Config file missing! Delete the dzr_rags_by_hand folder, restart server to create default. Or create the file manually: "+ m_Path+m_TxtFileName);
		}
		// READ FILE CONTENTS
		return content;
		
	}
}