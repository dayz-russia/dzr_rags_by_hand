class ActionTearIntoRagsCB : ActionContinuousBaseCB
{
	override void CreateActionComponent()
	{
		
		ref Param2<int, int> m_Config = GetDayZGame().DZRRBH_ReadServerConfig();
		m_ActionData.m_ActionComponent = new CAContinuousTime(m_Config.param1);
	}
};

class ActionTearIntoRags: ActionContinuousBase
{
	
	
		ref protected EffectSound m_TearingLoopSound;
	protected string m_TearingLoopSoundSet;
	
	void ActionTearIntoRags()
	{
		m_CallbackClass = ActionTearIntoRagsCB;
		m_CommandUID = DayZPlayerConstants.CMD_ACTIONFB_INTERACT;
		m_FullBody = true;
		m_StanceMask = DayZPlayerConstants.STANCEMASK_CROUCH;
		m_SpecialtyWeight = UASoftSkillsWeight.ROUGH_LOW;
		
		m_TearingLoopSoundSet 	= "TearUp_Soundset_start"; //Sound from Zapsplat.com
		
	}

	void ~ActionTearIntoRags()
	{
		DestroyTearingLoopSound();
	}

	override void CreateConditionComponents()
	{
		m_ConditionItem = new CCINonRuined;
		m_ConditionTarget = new CCTNone;
	}
	
	override bool HasTarget()
	{
		return false;
	}
	
	override string GetText()
	{
		return "#STR_CraftRag0";
	}
	override void OnStartAnimationLoopClient( ActionData action_data )
	{
		super.OnStartAnimationLoopClient(action_data);
		
		PlayTearingLoopSound( action_data );
	}
	
	override void OnEndAnimationLoopClient( ActionData action_data )
	{
		super.OnEndAnimationLoopClient(action_data);
		
		StopTearingLoopSound();
	}
	
	
	override bool ActionCondition( PlayerBase player, ActionTarget target, ItemBase item )
	{
		if( item.ConfigGetFloat("ragQuantity") > 0 && item.IsEmpty() && item.GetWet() <= (GameConstants.STATE_WET - 0.01) )
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	
		void PlayTearingLoopSound( ActionData action_data )
		{
		if ( GetGame().IsMultiplayer() && GetGame().IsClient() || !GetGame().IsMultiplayer() )
		{
			if ( !m_TearingLoopSound )
			{
				m_TearingLoopSound = new EffectSound;
			}
			if ( !m_TearingLoopSound.IsSoundPlaying() )
			{
				m_TearingLoopSound = SEffectManager.PlaySound( m_TearingLoopSoundSet, action_data.m_Player.GetPosition(),0,0,false );
				m_TearingLoopSound.SetSoundAutodestroy( true );
			}
		}
	}
	
	void StopTearingLoopSound()
	{
		if ( m_TearingLoopSound )
		{
			m_TearingLoopSound.SetSoundFadeOut(0.5);
			m_TearingLoopSound.SoundStop();
		}
	}
	
	
	void DestroyTearingLoopSound()
	{
		if ( m_TearingLoopSound )
		{
			SEffectManager.DestroySound( m_TearingLoopSound );
		}
	}
	
	
	override void OnFinishProgressServer( ActionData action_data )
	{		
		
		float quality = action_data.m_MainItem.GetHealth01("", "");
		
		int quantity = action_data.m_MainItem.ConfigGetFloat("ragQuantity");
		ItemBase oldItem = action_data.m_MainItem;
		
		ref Param2<int, int> m_Config = GetDayZGame().DZRRBH_ReadServerConfig();
		
		if(m_Config.param2 != 0)
		{
			ItemBase oldMaterial = ItemBase.Cast( GetGame().CreateObjectEx(oldItem.GetType(), action_data.m_Player.GetPosition(), ECE_PLACE_ON_SURFACE) );
			oldMaterial.SetHealth01( "", "", 0.0 );
		}
		
		int modifier = 0;
		
		if(quantity >= 2)
		{
			modifier = 1;
		};
		
		if(quantity >= 4)
		{
			modifier = 2;
		};
		
		MaterialLambda lambda = new MaterialLambda(oldItem, "Rag", action_data.m_Player, quantity - modifier, quality);
		action_data.m_Player.ServerReplaceItemInHandsWithNew(lambda);
		
		action_data.m_Player.GetSoftSkillsManager().AddSpecialty( m_SpecialtyWeight );
		action_data.m_Player.GetStaminaHandler().SetStamina(0);
		
		EntityAI inHands = action_data.m_Player.GetHumanInventory().GetEntityInHands();
		}
		
};

class MaterialLambda : ReplaceItemWithNewLambdaBase
{
	
	int m_Quantity;
	float m_Quality;
	
	void MaterialLambda(EntityAI old_item, string new_item, PlayerBase player, int count, float quality) 
	{
		m_Quantity = count; 
		m_Quality = quality; 
	}
	
	override void CopyOldPropertiesToNew (notnull EntityAI old_item, EntityAI new_item)
	{
		super.CopyOldPropertiesToNew(old_item, new_item);
		
		ItemBase theItem;
		Class.CastTo(theItem, new_item);
		theItem.SetQuantity(m_Quantity);
		theItem.SetHealth01("", "", m_Quality);
	}
};
