class CfgPatches
{
	class dzr_rags_by_hand_Scripts
	{
		requiredAddons[] = {"DZ_Data"};
		units[] = {};
		weapons[] = {};
	};
};
class CfgMods
{
	class dzr_rags_by_hand
	{
		type = "mod";
		author = "DayZ Russia";
		dir = "dzr_rags_by_hand";
		name = "dzr_rags_by_hand";
		dependencies[] = {"World","Game","Mission"};
		class defs
		{
			class worldScriptModule
			{
				files[] = {"dzr_rags_by_hand/4_World"};
			};			
			class gameScriptModule
			{
				files[] = {"dzr_rags_by_hand/3_Game"};
			};
			class missionScriptModule
			{
				files[] = {"dzr_rags_by_hand/5_Mission"};
			};
		};
	};
};
class CfgSoundShaders
{
	class TearUp_BaseSound
	{
		range=80;
		volume=2;
		distance=9;
	};
	class TearUp_Shader_CO8: TearUp_BaseSound
	{
		volume=1.8;
		samples[]=
		{
			
			{
				"\dzr_rags_by_hand\sounds\TearIntoRags",
				1
			}
		};
	};
};
class CfgSoundSets
{
	class TearUp_Soundset_baseSound
	{
		distance=5;
		loop=0;
		spatial=1;
	};
	class TearUp_Soundset_start: TearUp_Soundset_baseSound
	{
		soundShaders[]=
		{
			"TearUp_Shader_CO8"
		};
		frequencyRandomizer=0;
		volumeRandomizer=0;
		volumeFactor=1;
	};
};